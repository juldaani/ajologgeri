Prototyyppisovellus kuljetusliikkeelle ajopäiväkirjan ja kuljetun reitin tallentamiseksi mobiililaitteella (Android).

**Sovellus**
 
* Trackkaa mobiililaitteen sijaintia ja tutkii ollaanko liikkeessä vai paikallaan. Kun sovellus havaitsee, että liiiketila vaihtuu liikkestä pysähdyksiin, niin käyttäjältä kysytään tietoja pysähdyksestä (esim. lastaus, purku, kahvitauko jne).

* Tallentaa kuljetun reitin .gpx -muodossa

* Kuljettu reitti ja ajopäiväkirja tallennetaan Dropboxiin

* Mahdollisuus määrittää etukäteen alueita, joiden sisälle mentäessä jokin tietty toiminto käynnistetään sovelluksessa.