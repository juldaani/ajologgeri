// Apuluokka informaation palauttamiseksi ProcessLocations -luokasta
// Juho Uusi-Luomalahti, TTY Porin laitos		8.7.2014

package fi.tut.pori.ajologgeri;

import android.location.Location;

public class AnalyzedData {
	private Location loc;
	private boolean isMoving, isInsideGeofence;
	private String place, activity;
	private double distBetweenCurAndPrevLoc;
	
	public AnalyzedData(Location loc, boolean isMoving, boolean isInsideGoefence, String place, String activity,
			double distBetweenCurAndPrevLoc){
		this.isMoving = isMoving;
		this.loc = loc;
		this.activity = activity;
		this.place = place;
		this.isInsideGeofence = isInsideGoefence;
		this.distBetweenCurAndPrevLoc = distBetweenCurAndPrevLoc;
	}
	
	public AnalyzedData(Location loc, String place, String activity){
		this.loc = loc;
		this.activity = activity;
		this.place = place;
	}
	
	public double getDistBetweenCurAndPrevLoc(){
		return distBetweenCurAndPrevLoc;
	}
	
	public String getActivity(){
		return activity;
	}
	
	public String getPlace(){
		return place;
	}

	public Location getLoc() {
		return loc;
	}

	public boolean isMoving() {
		return isMoving;
	}
	
	public boolean isInsideGeofence(){
		return isInsideGeofence;
	}
	
	
}
