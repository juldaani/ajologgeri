// Konvertoi reittitiedoston .gpx muotoon ja lataa sen Dropboxiin
// TTY, Porin laitos / Juho Uusi-Luomalahti			6.8.2014

package fi.tut.pori.ajologgeri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.StringTokenizer;

import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.dropbox.sync.android.DbxException.Unauthorized;

import android.content.Context;
import android.location.Location;
import android.os.Environment;
import android.widget.Toast;

public class GPXRouteFileWriter {

	private String gpxFileName;
	private LinkedList<RoutePoint> routePointList;		// List for route tracking
	
	private boolean isLinked;	
	private DbxAccountManager mDbxAcctMgr;
	private DbxFileSystem dbxFs; 
	
	
	//*************************
	// Constructor
	//************************
	public GPXRouteFileWriter(Context ctx, String curLogFileName, String curRouteFileName){
		// Dropbox definitions
		mDbxAcctMgr = DbxAccountManager.getInstance(ctx, Parameters.APP_KEY, Parameters.APP_SECRET);
		isLinked = mDbxAcctMgr.hasLinkedAccount();
		if(isLinked){
			try {
				dbxFs = DbxFileSystem.forAccount(mDbxAcctMgr.getLinkedAccount());
			} catch (Unauthorized e) {
				e.printStackTrace();
			}
		}
		else{
			Toast.makeText(ctx, "Sovellusta ei ole yhdistetty Dropboxiin!", 
					Toast.LENGTH_SHORT).show();
		}
		
		gpxFileName = "Route_" + curLogFileName + ".gpx";
		createNewGpxFile();
		routePointList = new LinkedList<RoutePoint>();
		parseRouteFile(ctx, curRouteFileName);
		write2GpxFile(ctx);
	}

	
    //***************************************
    // Create new gpx route file
    //***************************************
    private void createNewGpxFile(){
    	File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, gpxFileName);
		
		try{
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			
			myOutWriter.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
			myOutWriter.append("<gpx creator=\"SIGALA, ajologgeri\" version=\"1.0\"> \n");
			
			myOutWriter.close();
			fOut.close();
		}
		catch(Exception e){}
    }
    
    
    //**********************************
    // Parse information from a route file and put it to linked list (routePointList)
    //***********************************
    private void parseRouteFile(Context ctx, String curRouteFileName){
    	
    	try{
    		File externalStorageDir = Environment.getExternalStorageDirectory();
        	File dir = new File (externalStorageDir + "/lokit/");
        	
    		File myFile = new File(dir, curRouteFileName);
    		FileReader fileReader = new FileReader(myFile);
    		BufferedReader bufferedReader = new BufferedReader(fileReader);
    		
    		String readLine;
		
		while((readLine = bufferedReader.readLine()) != null){
			
			// Read first character, 
			// Skips description part at the beginning of the route file
			String firstChar = null;
			if( (readLine != null) && (readLine.length() > 2) ){
				firstChar = readLine.substring(0, 1);
			}
			
			// If the first character is equals to @, then a line contains information about stop
			if( firstChar.equals("@") ){
				// Parse
				StringTokenizer st = new StringTokenizer(readLine, "#", false);
				String tmpLat = st.nextToken();
				tmpLat = tmpLat.substring(1);
				double lat = Double.parseDouble(tmpLat);
				double lon = Double.parseDouble(st.nextToken());
				String place = st.nextToken();
				String activity = st.nextToken();
				String stopStartTime = st.nextToken();
				String stopEndTime = st.nextToken();
				
				// Add point to list
				Location loc = new Location("tmp");
				loc.setLatitude(lat);
				loc.setLongitude(lon);
				RoutePoint routePoint = new RoutePoint(loc, true, place, activity, stopStartTime, stopEndTime);
				routePointList.addFirst(routePoint);
			}
			
			// If the first character is equals to #, then a line contains information about
			// some coordinate point (means that device was currently moving)
			else if( firstChar.equals("#") ){
				// Parse
				StringTokenizer st = new StringTokenizer(readLine, "#", false);
				double lat = Double.parseDouble(st.nextToken());
				double lon = Double.parseDouble(st.nextToken());
				
				// Add point to list
				Location loc = new Location("tmp");
				loc.setLatitude(lat);
				loc.setLongitude(lon);
				RoutePoint routePoint = new RoutePoint(loc, false, "", "", "", "");
				routePointList.addFirst(routePoint);
			}
		}
		
		bufferedReader.close();
    	}
    	catch(Exception e){
    		Toast.makeText(ctx, "Reittitiedoston luku epäonnistui.", Toast.LENGTH_SHORT).show();
    	}
    }
    
    
    //*****************************************
    // Write information from routePointList to gpx file
    //********************************************
    private void write2GpxFile(Context ctx){
    	// Write on the gpx file
		File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, gpxFileName);
		try {
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			
			// Iterate through list and find stop points
			for (RoutePoint routP : routePointList) {	
				double lat = routP.getLoc().getLatitude();
				double lon = routP.getLoc().getLongitude();
				
				// If device has been stopped in that point
				// Add wpt to gpx file
				if( routP.isStopPoint() ){
					//myOutWriter.append("@" + lat + "#" + lon + "# " + routP.getPlace() + "# " + routP.getActivity() 
					//		+ "# " + routP.getStopStartTime() + "# " + routP.getStopEndTime() + "#\n");
					myOutWriter.append("\t<wpt lat=\"" + lat + "\" " +  "lon=\"" + lon + "\">"
							+ "<name>" + routP.getPlace() + ", " + routP.getActivity() + " "
							+ routP.getStopStartTime() + "-" + routP.getStopEndTime() + "</name></wpt>\n");
				}
			}
			
			myOutWriter.append("\t<trk>\n");
			myOutWriter.append("\t\t<name>Track</name>\n");
			myOutWriter.append("\t\t<trkseg>\n");
			
			// Iterate through list and find points where device was currently moving
			for (RoutePoint routP : routePointList) {	
				double lat = routP.getLoc().getLatitude();
				double lon = routP.getLoc().getLongitude();
				
				// If device has been moving at that point
				// Add trkpt to gpx file
				if( routP.isStopPoint() == false ){
					myOutWriter.append("\t\t\t<trkpt lat=\"" + lat + "\" " + "lon=\"" + lon + "\"></trkpt>\n");
				}
			}
			
			myOutWriter.append("\t\t</trkseg> \n");
			myOutWriter.append("\t</trk>\n");
			myOutWriter.append("</gpx>");
			
			myOutWriter.close();
			fOut.close();
		} catch (Exception e) {
			Toast.makeText(ctx, "Reittitiedoston kirjoitus epäonnistui", Toast.LENGTH_SHORT).show();
		}
		
		// Dropbox
    	// Create new route gpx file to Dropbox. Upload from external storage to dropbox
		try {
			DbxPath dirDbx = new DbxPath("/lokit");
			DbxFile gpxFile = dbxFs.create( new DbxPath(dirDbx, gpxFileName) );
			
			// Get file from external storage
	    	File externalStorageDir2 = Environment.getExternalStorageDirectory();
	    	File dir2 = new File (externalStorageDir2 + "/lokit/");
			File gpxExternalFile = new File(dir2, gpxFileName);
			
			gpxFile.writeFromExistingFile(gpxExternalFile, false);
			gpxFile.close();
		} catch (IOException e) {
			Toast.makeText(ctx, "Reittitiedoston lataus pilveen epäonnistui", Toast.LENGTH_SHORT).show();
		}
		
		
    }
    
}
