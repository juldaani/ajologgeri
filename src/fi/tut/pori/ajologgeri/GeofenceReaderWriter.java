package fi.tut.pori.ajologgeri;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import android.content.Context;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.dropbox.sync.android.DbxException.Unauthorized;
import com.dropbox.sync.android.DbxPath.InvalidPathException;
import com.google.android.gms.maps.model.LatLng;

public class GeofenceReaderWriter {
	
	private LinkedList<Geofence> geofenceList;	// List for geofences

	private boolean isLinked;	
	private DbxAccountManager mDbxAcctMgr;
	private DbxFileSystem dbxFs; 
	
	
	//***************
	// Constructor
	//****************
	public GeofenceReaderWriter(Context ctx){
        // Dropbox definitions
		mDbxAcctMgr = DbxAccountManager.getInstance(ctx, Parameters.APP_KEY, Parameters.APP_SECRET);
		isLinked = mDbxAcctMgr.hasLinkedAccount();
		if(isLinked){
			try {
				dbxFs = DbxFileSystem.forAccount(mDbxAcctMgr.getLinkedAccount());
			} catch (Unauthorized e) {
				e.printStackTrace();
			}
		}
		else{
			Toast.makeText(ctx, "Sovellusta ei ole yhdistetty Dropboxiin!", 
					Toast.LENGTH_SHORT).show();
		}
		
		geofenceList = new LinkedList<Geofence>();
		readGeofencesFromFile(ctx);
	}
	
	
	//*******************************************************************
    // Add new geofence to the geofenceList and write updated geofences to the file
    //*****************************************************************
    public void addGeofence(Context ctx, Geofence geofence){
    	geofenceList.addFirst(geofence);
    	writeGeofences2file(ctx);
    }
    
    
    //*******************************************************************
    // Remove a geofence from the geofenceList and write updated geofences to the file
    //*****************************************************************
    public void removeGeofence(Context ctx, LatLng clickedLatLng, String clickedPlace){
    	int index = findElementIndex(clickedLatLng, clickedPlace);
    	geofenceList.remove(index);
    	writeGeofences2file(ctx);
    }
	
	
	//****************************************************
    // Read geofences from Dropbox  
    // + add geofences to the geofenceList
    //*************************************************
    private void readGeofencesFromFile(Context context){
    	
		try {
			DbxFile testFile = dbxFs.open(new DbxPath("Geofences.txt"));
			String contents = testFile.readString();
			
			Scanner scanner = new Scanner(contents);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
			  
				// read data from the line to the variables
				StringTokenizer st = new StringTokenizer(line, "#", false); 
				double lat = Double.parseDouble(st.nextToken());
				double lon = Double.parseDouble(st.nextToken());
				LatLng latitLongit = new LatLng(lat, lon);
				int rad = Integer.parseInt(st.nextToken());
				String place = st.nextToken();
				String activity = st.nextToken();
				
				// add geofence to the geofenceList
				Geofence geof2add = new Geofence(latitLongit, activity, place, rad);
				geofenceList.addFirst(geof2add);
			}
			scanner.close();
			
    	    testFile.close();
		} catch (InvalidPathException | IOException e1) {
			e1.printStackTrace();
			Toast.makeText(context,
                    "Geofence -tiedoston avaus epäonnistui.", Toast.LENGTH_SHORT).show();
		}
    }
    
    
    //**********************************
    // Write geofences to the internal storage
    //*********************************
    private void writeGeofences2file(Context ctx){
    	
    	// Create geofences.txt to internal storage
    	try{
	    	BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new 
	                File(ctx.getFilesDir()+File.separator+"Geofences.txt")));
	    	
	    	// Iterate through the list and write geofences to txt file
	    	for (Geofence geof : geofenceList) {		//
	    		bufferedWriter.write(geof.getLatLng().latitude +"#"+ geof.getLatLng().longitude +"#"+
	    				geof.getRadius() +"#"+ geof.getPlace() +"#"+ geof.getActivity() +"#\n");
	    	}
	    	
			bufferedWriter.close();
    	}
    	catch(Exception e){
    		Toast.makeText(ctx, "Tallennus epäonnistui.", Toast.LENGTH_SHORT).show();
    	}
    	
		// Dropbox
    	// Delete existing geofences.txt
    	try {
			dbxFs.delete(new DbxPath("Geofences.txt"));
		} catch (InvalidPathException | DbxException e1) {
			e1.printStackTrace();
		}
    	
		// Dropbox
    	// Create new geofences.txt. Upload from internal storage to dropbox
		try {
			DbxFile geofFile = dbxFs.create( new DbxPath("Geofences.txt") );
			
			File geofInternalFile = new File(ctx.getFilesDir()+File.separator+"Geofences.txt");
			
			geofFile.writeFromExistingFile(geofInternalFile, false);
			geofFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    }
    
    
    //*********************************************
    // Find geofence from the geofenceList (search criterias: place, latlng)
    //***********************************************
    public Geofence findGeofence(LatLng latLng, String place){
    	int index = findElementIndex(latLng, place);
    	return geofenceList.get(index);
    }
    
	
	// *************************************
	// Find index of the element from the geofenceList
	// ************************************
	private int findElementIndex(LatLng latLng, String place ){
		int index = 0;
		double lat = latLng.latitude;
		double lon = latLng.longitude;
		for (Geofence curGeof : geofenceList) {		//iterate through the locationList
			// If object found from the list then return index
			if( (curGeof.getPlace().equals(place)) && (curGeof.getLatLng().latitude == lat) 
					&& (curGeof.getLatLng().longitude == lon) ){
				return index;
			}
			index++;
		}
		return -1;
	}
	
	
	public LinkedList<Geofence> getGeofenceList(){
		return geofenceList;
	}
    
}
