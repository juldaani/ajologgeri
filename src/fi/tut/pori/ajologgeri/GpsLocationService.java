// Service gps-tiedon tarjoamiseksi muille sovelluksen osille
// Juho Uusi-Luomalahti, TTY Porin laitos		10.7.2014

package fi.tut.pori.ajologgeri;

import java.util.Calendar;
import java.util.LinkedList;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;


public class GpsLocationService extends Service {
    // Binder given to clients for bounding to the service
    private final IBinder serviceBinder = new LocalBinder();
    
    private LocationManager locationManager;
    private LocationListener locationListener;
    
    private boolean isGpsEnabled;
    private boolean isInput;
    private boolean prevIsMoving, curIsMoving;
    
    private AnalyzedData analyzedLocData;
    
    private String stopStartTime, stopEndTime;
    //private double stopCoordLat, stopCoordLon;
    private Location stopLocation;
    
    private String curInputPlace, curInputActivity;
    
    LogFileWriter currentLog;
    RouteFileWriter currentRoute;
    
    GeofenceReaderWriter geofences;
    
    private double overallDist;
    
    @Override
	public void onCreate(){
        final ProcessLocations ProcessLocations = new ProcessLocations();
        
        overallDist = 0.0;
        
        isGpsEnabled = false;
        isInput = false;
        
        prevIsMoving = true;
        curIsMoving = true;
        
        geofences = new GeofenceReaderWriter(getApplicationContext());
        
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        
        //	Check is gps enabled
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            isGpsEnabled = true;
        }
        
        
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            
        	public void onLocationChanged(Location location) {
            	if(isGpsEnabled){
            		
                    analyzedLocData = ProcessLocations.inputLocation(location, geofences.getGeofenceList() );
                    
                    prevIsMoving = curIsMoving;
                    curIsMoving = analyzedLocData.isMoving();
                    
                    overallDist += analyzedLocData.getDistBetweenCurAndPrevLoc();
                    
                    // If device has just stopped moving, then start InputEventActivity
                    if( (prevIsMoving == true) && (curIsMoving == false) ){
                    	createNotification();
                    	stopStartTime = getCurTime();
                    	stopLocation = analyzedLocData.getLoc();
                    	//stopCoordLat = analyzedLocData.getLoc().getLatitude();
                    	//stopCoordLon = analyzedLocData.getLoc().getLongitude();
                    	
                    	// Start activity
                    	Intent i = new Intent(getBaseContext(), InputEventActivity.class);
                    	i.putExtra("PLACE", analyzedLocData.getPlace());
                    	i.putExtra("ACTIVITY", analyzedLocData.getActivity());
                		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                    
                    // If device has just started to move
                    // - send broadcast to shut down InputEventActivity
                    // - write timestamp and coordinates for the stop to a log file
                    // - add RoutePoint to the routePointList
                    else if( (prevIsMoving == false) && (curIsMoving == true) ){
                    	deleteNotification();
                    	stopEndTime = getCurTime();
                    	
                    	RoutePoint routePoint = new RoutePoint(stopLocation, true, "", "", stopStartTime,
                    			stopEndTime);
                    	
                    	// If user has added some input for the stop, then we know that this stop should be added
                    	// to a log file
                    	if(isInput){
                    		isInput = false;
                    		//write to a logfile
                    		currentLog.writeTimeAndCoordinates(getApplicationContext(), stopStartTime, stopEndTime, 
                    				stopLocation.getLatitude(), stopLocation.getLongitude(), overallDist);	
                    		routePoint = new RoutePoint(stopLocation, true, curInputPlace, curInputActivity, 
                    				stopStartTime, stopEndTime);
                    	}
                    	
                    	// Send broadcast to close InputEventActivity (InputEventActivity should be closed when moving)
                    	String action = "STATE_STARTED_MOVING";
                    	Intent intent = new Intent(action);
                    	sendBroadcast(intent);
                    	
                    	// Add point to the routePointList
                    	currentRoute.addRoutePoint2List(getApplicationContext(), routePoint);
                    }
                    
                    // If device is currently moving, then add just "empty" (only coordinates)
                    // RoutePoint to the routePointList
                    else if(curIsMoving){
                    	RoutePoint routePoint = new RoutePoint(analyzedLocData.getLoc(), false, "", "", "", "");
                    	currentRoute.addRoutePoint2List(getApplicationContext(), routePoint);
                    }
                    
                    // if device is still (for debugging)
                    if( !analyzedLocData.isMoving() ){
                    	Toast.makeText(getApplicationContext(), "Stopped!", Toast.LENGTH_SHORT).show();
                    }
                                        
            	}
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
          };
        
        
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager. GPS_PROVIDER, Parameters.LOC_UPDATE_INTERVAL,
        		0, locationListener);
        
    }
    
    
    @Override
    public void onDestroy(){
    	locationManager.removeUpdates(locationListener);
    	deleteNotification();
    	currentRoute.write2RouteFile(getApplicationContext());
    	// Create gpx file
    	new GPXRouteFileWriter(getApplicationContext(), 
    			currentLog.getLogFileName(), currentRoute.getRouteFileName());
    	
    }
    
    
    //*******************************
    // Delete notification from status bar
    //*******************************
    private void deleteNotification(){
		String ns = Context.NOTIFICATION_SERVICE;
	    NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
	    nMgr.cancel(0);
    }
    
    
    //******************************
    // Get current time
    //******************************
    private String getCurTime(){
    	Calendar c = Calendar.getInstance();
    	int min = c.get(Calendar.MINUTE);
    	int hour = c.get(Calendar.HOUR_OF_DAY);
    	return hour +":"+ min;
    }

    
    //*************************************
    // Write description for the stop
    // called from InputEventActivity
    //**************************************
    public void writeStopDescription(Context context, String paikka, String tapahtuma){
    	curInputPlace = paikka;
    	curInputActivity = tapahtuma;
		isInput = true;
		currentLog.writeStopDescription(context, paikka, tapahtuma);
    }
    
    
    //**************************
    // Create notification 
    //***************************
     private void createNotification(){
    	 NotificationCompat.Builder mBuilder =
    			    new NotificationCompat.Builder(this)
    			    .setSmallIcon(R.drawable.ic_launcher)
    			    .setContentTitle("AjoLoggeri")
    			    .setContentText("Lis�� merkint�")
    			    .setPriority(2);
    			    //.setAutoCancel(true);
    	 Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    	 mBuilder.setSound(notificationSound);
    	 
    	 Intent resultIntent = new Intent(this, InputEventActivity.class);
    	 PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent,
    			 PendingIntent.FLAG_UPDATE_CURRENT);
    	 mBuilder.setContentIntent(resultPendingIntent);
    	 NotificationManager mNotificationManager =
    			    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    	 mNotificationManager.notify(0, mBuilder.build());
     }
    
    
    // ******************************************
    // Class for defining binding interface for clients
    //********************************************
    public class LocalBinder extends Binder {
    	GpsLocationService getService() {
            // Return this instance of the service so clients can call public methods
            return GpsLocationService.this;
        }
    }
    
    
    public void setIsInput(boolean state){
    	isInput = state;
    }
    
    public AnalyzedData getAnalyzedData(){
    	return analyzedLocData;
    }
    
    public String getCurLogFileName(){
    	return currentLog.getLogFileName();
    }
    
	public LinkedList<Geofence> getGeofenceList(){
		return geofences.getGeofenceList();
	}
	
	// Called from MainActivity after the service is started
    public void createNewLogFile(String logDesc, String driver, String vehicle, String cargoDet,
    		String cargoNum){
    	currentLog = new LogFileWriter(logDesc, driver, vehicle, cargoDet, cargoNum);
    	currentRoute = new RouteFileWriter(currentLog.getLogFileName());
    }
    
    
    public double getOverallDist(){
    	return overallDist;
    }
    
    public String getCurRouteFileName(){
    	return currentRoute.getRouteFileName();
    }
    
    
    // *********************************
    // Method for writing information to the log file for the last stop. 
    // Used in the case when device is not moving and logging is stopped by the user
    // Without this last stop would not have time and coordinates
    // **********************************
    public void writeInformationForLastStop(){
    	if( isInput ){
    		stopEndTime = getCurTime();
    		currentLog.writeTimeAndCoordinates(getApplicationContext(), stopStartTime, stopEndTime,
    				stopLocation.getLatitude(), stopLocation.getLongitude(), overallDist);
    	}
    }
    
	
    //*******************************************
    // Method for clients for binding to the service
    //*******************************************
    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }
    
}

