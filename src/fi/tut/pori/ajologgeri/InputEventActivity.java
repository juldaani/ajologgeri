// Aktivity tietojen syöttämiseksi kun on pysähdytty
// Juho Uusi-Luomalahti, TTY Porin laitos		15.7.2014

package fi.tut.pori.ajologgeri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import fi.tut.pori.ajologgeri.GpsLocationService.LocalBinder;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class InputEventActivity extends Activity{
	
	private GpsLocationService gpsLocationService;
    private boolean mBound;
    
    private String curLogFileName;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.input_event);
		
		mBound = false;
		
		// Get data from intent. PLACE and ACTIVITY carries information about geofence (if device 
		// is stopped inside). If is not inside any geofence, then PLACE and ACTIVITY are empty.
		Intent i = getIntent();
		String place = i.getStringExtra("PLACE");
		String activity = i.getStringExtra("ACTIVITY");
		
		// Set information about geofence to EditTexts
    	EditText editText1 = (EditText)findViewById(R.id.paikka_editText);
    	editText1.setText(place);
        EditText editText2 = (EditText)findViewById(R.id.tapahtuma_editText);
        editText2.setText(activity);
        
		// Register broadcast receiver for listening broadcasts from GpsLocationService
		IntentFilter filter = new IntentFilter();
		filter.addAction("STATE_STARTED_MOVING");
	    registerReceiver(shutdownActivityReceiver, filter);
	    
    	// Bind to GpsLocationService
        Intent intent = new Intent(this, GpsLocationService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        
	}
	
	
	//*********************************************
	// If user has moved activity to background, then activity doesn't listen broadcasts. So the activity has to check
	// from GpsLocationService is mobile device moving
	// if moving --> finish activity
	//*********************************************
	@Override
	protected void onResume() {
		super.onResume();
		boolean isMoving = false;
		if(mBound && (gpsLocationService.getAnalyzedData() != null) ){
			isMoving = gpsLocationService.getAnalyzedData().isMoving();
		}
		if(isMoving){
			finish();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		try{
			unregisterReceiver(shutdownActivityReceiver);
		}
		catch(Exception e){}
		
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		try{
			unregisterReceiver(shutdownActivityReceiver);
		}
		catch(Exception e){}
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
    	
		if (mBound) {
	    	unbindService(mConnection);
	        mBound = false;
	        Toast.makeText(getApplicationContext(), "Unbinded", Toast.LENGTH_SHORT).show();
    	}
    	
		try{
			unregisterReceiver(shutdownActivityReceiver);
		}
		catch(Exception e){}
	}
	
	
	// ************************************
	// Method for button click. Write information to txt file.
	//*************************************
	public void inputButtonClick(View view){
    	EditText editText1 = (EditText)findViewById(R.id.paikka_editText);
        String paikka = editText1.getText().toString();
        EditText editText2 = (EditText)findViewById(R.id.tapahtuma_editText);
        String tapahtuma = editText2.getText().toString();
        
        // Write inputs to the log file
        gpsLocationService.writeStopDescription(this, paikka, tapahtuma);
		
		editText1.setText("");
		editText2.setText("");
	    
		finish();
	}
	
	
    //*************************************************
    // Callback for binding/unbinding to the service
    // *************************************************
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalBinder binder = (LocalBinder) service;
            gpsLocationService = binder.getService();
            mBound = true;
            curLogFileName = gpsLocationService.getCurLogFileName();
            Toast.makeText(getApplicationContext(), "Binded!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    
		
    // *****************************************************************************
	// Receiver for listening when GpsLocationService tells that device is moving.
    // when moving --> finish this activity
	// *****************************************************************************
    private final BroadcastReceiver shutdownActivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	finish();                                   
        }
	};
}

