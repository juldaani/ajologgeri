// Luokka hoitaa lokitiedostoon kirjoituksen
// Juho Uusi-Luomalahti, TTY Porin laitos		1.8.2014

package fi.tut.pori.ajologgeri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

public class LogFileWriter {

	private String curLogFileName, curLogDescription, curDriver, curVehicle, curCargoDetails, curCargoNumber;
	
	
	//******************
	// Constructor
	//**********************
	public LogFileWriter(String curLogDescription, String curDriver, 
			String curVehicle, String curCargoDetails, String curCargoNumber){
		this.curLogDescription = curLogDescription;
		this.curDriver = curDriver;
		this.curVehicle = curVehicle;
		this.curCargoDetails = curCargoDetails;
		this.curCargoNumber = curCargoNumber;
		
		curLogFileName = genLogName();
		createNewLogFile();
	}
	
	
    //********************************************
    // Generate name for new log file
    //*******************************************+
    private String genLogName(){
    	Calendar c = Calendar.getInstance();
    	int min = c.get(Calendar.MINUTE);
    	int hour = c.get(Calendar.HOUR_OF_DAY);
    	int day = c.get(Calendar.DAY_OF_MONTH);
    	int month = c.get(Calendar.MONTH);
    	int year = c.get(Calendar.YEAR);
    	return "Ajoloki_" + day + "-" +month + "-" + year + "_" + hour + "-" + min + 
    			"_" + curDriver + "_" + curVehicle + "_" + curCargoNumber;
    }
    
    
    //*************************************
    // Get current date
    //**************************************
    private String getDate(){
    	Calendar c = Calendar.getInstance();
    	int day = c.get(Calendar.DAY_OF_MONTH);
    	int month = c.get(Calendar.MONTH);
    	int year = c.get(Calendar.YEAR);
    	return day + "." +month + "." + year;
    }
	
	
	//***************************************
    // Create new log file
    //***************************************
    private void createNewLogFile(){
    	File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, curLogFileName + ".txt");
		
		try{
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			
			myOutWriter.append("******************************************\n");
			myOutWriter.append("AJOLOKI\t\t" + getDate() + "\n");
			myOutWriter.append(curLogDescription + "\n");
			myOutWriter.append(curDriver + ", " + curVehicle + "\n\n");
			myOutWriter.append("Kuorma:\t" + curCargoDetails + "\n");
			myOutWriter.append("Nro:\t" + curCargoNumber + "\n");
			myOutWriter.append("******************************************");
			myOutWriter.append("\n\n");
			
			myOutWriter.close();
			fOut.close();
		}
		catch(Exception e){
		}
    }
    
    
    //******************************
    // Write timestamp and coordinates to a logfile
    // Called when device starts moving after stop
    //*****************************
    public void writeTimeAndCoordinates(Context context, String stopStartTime, String stopEndTime, 
    		double stopCoordLat, double stopCoordLon, double overallDist){
        // Write on a file
		File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, curLogFileName + ".txt");
		
		try {
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			
			myOutWriter.append("Aika:\t\t"); 
			myOutWriter.append(stopStartTime);
			myOutWriter.append(" - ");
			myOutWriter.append(stopEndTime  +"\n");
			
			// formatting lat/lon doubles
			NumberFormat df = DecimalFormat.getInstance();
			df.setMinimumFractionDigits(6);
			df.setMaximumFractionDigits(8);
			df.setRoundingMode(RoundingMode.HALF_UP);
			
			myOutWriter.append("Koordinaatit:\t");
			String lat = df.format(stopCoordLat);
			myOutWriter.append(lat +" " );
			String lon = df.format(stopCoordLon);
			myOutWriter.append(lon +"\n" );
			
			// formatting distance double
			NumberFormat distf = DecimalFormat.getInstance();
			distf.setMinimumFractionDigits(2);
			distf.setMaximumFractionDigits(2);
			distf.setRoundingMode(RoundingMode.HALF_UP);
			
			myOutWriter.append("Km alusta:\t");
			String dist = distf.format(overallDist);
			myOutWriter.append(dist +"\n");
			myOutWriter.append("\n");
			
			myOutWriter.close();
			fOut.close();
		} 
		catch (Exception e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
					.show();
		}
    }
    
    
    //*****************************************
    // Write stop descriptions from the InputEventActivity to the log file
    //********************************************
    public void writeStopDescription(Context context, String paikka, String tapahtuma){

    	// Write on a file
		File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, curLogFileName + ".txt");
		try {
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			myOutWriter.append(paikka);
			myOutWriter.append(", ");
			myOutWriter.append(tapahtuma +"\n");
			myOutWriter.close();
			fOut.close();
			
	        Toast.makeText(context, "Lis�tty lokiin", Toast.LENGTH_SHORT).show();
	        
		} catch (Exception e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
					.show();
		}
    }
    
    public String getLogFileName(){
    	return curLogFileName;
    }
}
