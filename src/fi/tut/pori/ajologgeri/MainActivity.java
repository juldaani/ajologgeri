// Android-sovellus, ajop�iv�kirja kuljetusliikkeelle
// TTY, Porin laitos / Juho Uusi-Luomalahti			2.7.2014

package fi.tut.pori.ajologgeri;


import java.io.File;
import java.io.IOException;

import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;

import fi.tut.pori.ajologgeri.GpsLocationService.LocalBinder;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    
	GpsLocationService gpsLocationService;
    private boolean mBound = false;

	private String curLogFileName, logDescription, vehicle, driver, cargoDetails, cargoNumber;
	
	// for dropbox
	private boolean isLinked;	
	private DbxAccountManager mDbxAcctMgr;
	private DbxFileSystem dbxFs; 
	private static final int REQUEST_LINK_TO_DBX = 0;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Dropbox definitions
		mDbxAcctMgr = DbxAccountManager.getInstance(getApplicationContext(), 
				Parameters.APP_KEY, Parameters.APP_SECRET);
		isLinked = mDbxAcctMgr.hasLinkedAccount();
		// link to dropbox
		if(!isLinked){
			mDbxAcctMgr.startLink((Activity)this, REQUEST_LINK_TO_DBX);
		}
				
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        //	Check is gps enabled
        if( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ){
        	showGPSDisabledAlertToUser();
        }
        
        // Check if external storage is writable and available
        checkExternalMedia();
        if( (mExternalStorageAvailable == false) || (mExternalStorageWriteable == false) ){
            Toast.makeText(getApplicationContext(), "External storage not available/writable", 
            		Toast.LENGTH_LONG).show();
            Button newLogButton = (Button) findViewById(R.id.newLog_button);
            newLogButton.setVisibility(View.GONE);
        }
    }
    
    
    //*******************************************
    // Show alert box when back button is pressed
    //******************************************
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        	createShutdownAlertBox();
        }
        return super.onKeyDown(keyCode, event);
    }
    
    
    //******************************************
    // Create shutdown alert box
    //*****************************************
    private void createShutdownAlertBox(){
        AlertDialog.Builder alertbox = new AlertDialog.Builder(MainActivity.this);
        alertbox.setTitle("Haluatko varmasti lopettaa?");
        alertbox.setPositiveButton("Kyll�", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) { 
            	
            	// If logging is currently running
            	if(mBound){
            		executeStopTrackingOperations();
            	}
            	// If logging is currently not running
            	else{
            		finish();
            	}
            	
            }
        });

        alertbox.setNegativeButton("Ei", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            	// if clicked no, do nothing
          }
        });

        alertbox.show();
    }
    
    
    //*********************************
    // Callback from dropbox linking method
    //********************************
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == REQUEST_LINK_TO_DBX) {
	        
	    	if (resultCode == Activity.RESULT_OK) {
	        	Toast.makeText(getApplicationContext(), "Linked to Dropbox", 
	            		Toast.LENGTH_LONG).show();
				// Set max cache size. Once set, the setting will remain in effect across app restarts.
	        	try {
					dbxFs = DbxFileSystem.forAccount(mDbxAcctMgr.getLinkedAccount());
					dbxFs.setMaxFileCacheSize(Parameters.MAX_CACHE);
				} catch (DbxException e) {
					e.printStackTrace();
				}
	        	
	        } else {
	        	Toast.makeText(getApplicationContext(), "Failed to link Dropbox", 
	            		Toast.LENGTH_LONG).show();
	        	
	        	// Hide buttons and show error text
	        	Button startServiceButton = (Button) findViewById(R.id.startService_button);
	            startServiceButton.setVisibility(View.GONE);
	            
	            Button newLogButton = (Button) findViewById(R.id.newLog_button);
	            newLogButton.setVisibility(View.GONE);
	            
	            Button stopLogButton = (Button) findViewById(R.id.stopService_button);
	            stopLogButton.setVisibility(View.GONE);
	            
	            Button routeOnMapButton = (Button) findViewById(R.id.routeOnMap_button);
	            routeOnMapButton.setVisibility(View.GONE);
	            
	            TextView dropboxMsg = (TextView) findViewById(R.id.dropbox_textView);
	            dropboxMsg.setVisibility(View.VISIBLE);
	        }
	    } else {
	        super.onActivityResult(requestCode, resultCode, data);
	    }
	}
    
    
    /**
     * Method to check whether external media available and writable. This is
     * adapted from
     * http://developer.android.com/guide/topics/data/data-storage.html
     * #filesExternal
     */
    boolean mExternalStorageAvailable;
    boolean mExternalStorageWriteable;
    
    private void checkExternalMedia() {
        mExternalStorageAvailable = false;
        mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
    }
    

    //*************************************
    // Show dialog if gps is not enabled
    //*************************************
    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
        .setCancelable(false)
        .setPositiveButton("Goto Settings Page To Enable GPS",
                new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
     
    
    //*******************************************
    // Start logging locations
    //******************************************    
    public void startServiceButtonClick(View view){
        
        // kuljettaja
        EditText kuljettajaEditText = (EditText) findViewById(R.id.kuljettaja_editText);
        driver = kuljettajaEditText.getText().toString();

        // auto
        EditText autoEditText = (EditText) findViewById(R.id.auto_editText);
        vehicle = autoEditText.getText().toString();

        // kuorman tiedot
        EditText kuormaEditText = (EditText) findViewById(R.id.kuormatiedot_editText);
        cargoDetails = kuormaEditText.getText().toString();

        // kuormanro
        EditText kuormanroEditText = (EditText) findViewById(R.id.kuormanro_editText);
        cargoNumber = kuormanroEditText.getText().toString();
        
        // ajon kuvaus
        EditText kuvausEditText = (EditText) findViewById(R.id.kuvaus_editText);
        logDescription = kuvausEditText.getText().toString();

        TextView lokinimiTextView = (TextView) findViewById(R.id.lokinimi_textView);
        lokinimiTextView.setVisibility(View.VISIBLE);
        
        // If some of the field is empty, then show alert dialog
        if( driver.equals("") || vehicle.equals("") || cargoDetails.equals("") || cargoNumber.equals("")
        		|| logDescription.equals("") ){

        	AlertDialog.Builder alert = new AlertDialog.Builder(this);
        	alert.setTitle("VAARA");
        	alert.setMessage("Tyhji� kentti�!");
        	alert.show();

        }
        else{
            Button startServiceButton = (Button) findViewById(R.id.startService_button);
            startServiceButton.setVisibility(View.GONE);
            
            Button newLogButton = (Button) findViewById(R.id.newLog_button);
            newLogButton.setVisibility(View.GONE);
            
            Button setOnMapButton = (Button) findViewById(R.id.setOnMap_button);
            setOnMapButton.setVisibility(View.GONE);
            
            Button stopLogButton = (Button) findViewById(R.id.stopService_button);
            stopLogButton.setVisibility(View.VISIBLE);
        	
        	// kuljettaja
            kuljettajaEditText.setVisibility(View.GONE);
            TextView kuljettaja2TextView = (TextView) findViewById(R.id.kuljettaja2_textView);
            kuljettaja2TextView.setText(driver);
            kuljettaja2TextView.setVisibility(View.VISIBLE);
            
            //auto
            autoEditText.setVisibility(View.GONE);
            TextView auto2TextView = (TextView) findViewById(R.id.auto2_textView);
            auto2TextView.setText(vehicle);
            auto2TextView.setVisibility(View.VISIBLE);
            
            //kuorman tiedot
            kuormaEditText.setVisibility(View.GONE);
            TextView kuormatiedot2TextView = (TextView) findViewById(R.id.kuormatiedot2_textView);
            kuormatiedot2TextView.setText(cargoDetails);
            kuormatiedot2TextView.setVisibility(View.VISIBLE);
            
            //kuorman nro
            kuormanroEditText.setVisibility(View.GONE);
            TextView kuormanro2TextView = (TextView) findViewById(R.id.kuormanro2_textView);
            kuormanro2TextView.setText(cargoNumber);
            kuormanro2TextView.setVisibility(View.VISIBLE);
            
            //ajon kuvaus
            kuvausEditText.setVisibility(View.GONE);
            TextView kuvaus2TextView = (TextView) findViewById(R.id.kuvaus2_textView);
            kuvaus2TextView.setText(logDescription);
            kuvaus2TextView.setVisibility(View.VISIBLE);
            
            // Bind to GpsLocationService
            Intent intent = new Intent(this, GpsLocationService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            
            // Start animation for target image
            ImageView img = (ImageView)findViewById(R.id.target_img);
            img.setVisibility(View.VISIBLE);
            img.setBackgroundResource(R.drawable.blink_anim);
            AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();
            frameAnimation.start();
            
    		// Hide keyboard
    		InputMethodManager inputMgr = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
    		inputMgr.hideSoftInputFromWindow(kuvausEditText.getWindowToken(), 0);
        }
        
	}
    
    
    //**********************************************
    // Stop logging button click
    //**********************************************    
    public void stopServiceButtonClick(View view){
    	createShutdownAlertBox();
    }
    
    
    //************************************************
    // Operations which are conducted after logging is stopped
    //**************************************************
    private void executeStopTrackingOperations(){
    	// If device is still then use special method to write timestamp and coordinates to a log file.
    	// (GpsLocationService writes information for stops only when device starts moving after stop)
    	// Without this method last stop would not have time and coordinates
    	gpsLocationService.writeInformationForLastStop();
    	
    	// UI manipulations
    	Button newLogButton = (Button) findViewById(R.id.newLog_button);
        newLogButton.setVisibility(View.VISIBLE);
        
        ImageView img = (ImageView)findViewById(R.id.target_img);
        img.setVisibility(View.GONE);
        
        Button stopLogButton = (Button) findViewById(R.id.stopService_button);
        stopLogButton.setVisibility(View.GONE);
        
        Button setOnMapButton = (Button) findViewById(R.id.setOnMap_button);
        setOnMapButton.setVisibility(View.VISIBLE);
    	
        // Unbind the service
    	if (mBound) {
	    	unbindService(mConnection);
	        mBound = false;
	        Toast.makeText(getApplicationContext(), "Paikannus pys�ytetty", Toast.LENGTH_SHORT).show();
    	}
    	
    	// Upload logfile to dropbox
		try {
			dbxFs = DbxFileSystem.forAccount(mDbxAcctMgr.getLinkedAccount());
			DbxPath dir = new DbxPath("/lokit");
			DbxFile logFile = dbxFs.create( new DbxPath(dir, curLogFileName + ".txt") );
			
	    	File externalStorageDir = Environment.getExternalStorageDirectory();
	    	File dir2 = new File (externalStorageDir + "/lokit/");
			File newFile = new File(dir2, curLogFileName + ".txt");
			
			logFile.writeFromExistingFile(newFile, false);
			logFile.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    
    //*************************************************
    // Create new log button
    // *************************************************
    public void newLogButtonClick(View view){
    	// UI manipulations
    	// kuljettaja
    	TextView kuljettajaTextView = (TextView) findViewById(R.id.kuljettaja_textView);
    	kuljettajaTextView.setVisibility(View.VISIBLE);
    	EditText kuljettajaEditText = (EditText) findViewById(R.id.kuljettaja_editText);
    	kuljettajaEditText.setText("");
    	kuljettajaEditText.setVisibility(View.VISIBLE);
        TextView kuljettaja2TextView = (TextView) findViewById(R.id.kuljettaja2_textView);
        kuljettaja2TextView.setVisibility(View.GONE);
    	
    	// auto
    	TextView autoTextView = (TextView) findViewById(R.id.auto_textView);
    	autoTextView.setVisibility(View.VISIBLE);
    	EditText autoEditText = (EditText) findViewById(R.id.auto_editText);
    	autoEditText.setText("");
    	autoEditText.setVisibility(View.VISIBLE);
        TextView auto2TextView = (TextView) findViewById(R.id.auto2_textView);
        auto2TextView.setVisibility(View.GONE);
    	
    	// kuorma
    	TextView kuormaTextView = (TextView) findViewById(R.id.kuormatiedot_textView);
    	kuormaTextView.setVisibility(View.VISIBLE);
    	EditText kuormaEditText = (EditText) findViewById(R.id.kuormatiedot_editText);
    	kuormaEditText.setText("");
    	kuormaEditText.setVisibility(View.VISIBLE);
        TextView kuorma2TextView = (TextView) findViewById(R.id.kuormatiedot2_textView);
        kuorma2TextView.setVisibility(View.GONE);
    	
    	// kuorma/rahtikirja nro
    	TextView kuormanroTextView = (TextView) findViewById(R.id.kuormanro_textView);
    	kuormanroTextView.setVisibility(View.VISIBLE);
    	EditText kuormanroEditText = (EditText) findViewById(R.id.kuormanro_editText);
    	kuormanroEditText.setText("");
    	kuormanroEditText.setVisibility(View.VISIBLE);
        TextView kuormanro2TextView = (TextView) findViewById(R.id.kuormanro2_textView);
        kuormanro2TextView.setVisibility(View.GONE);
    	
    	// ajon kuvaus
    	TextView kuvausTextView = (TextView) findViewById(R.id.kuvaus_textView);
    	kuvausTextView.setVisibility(View.VISIBLE);
    	EditText kuvausEditText = (EditText) findViewById(R.id.kuvaus_editText);
    	kuvausEditText.setText("");
    	kuvausEditText.setVisibility(View.VISIBLE);
        TextView kuvaus2TextView = (TextView) findViewById(R.id.kuvaus2_textView);
        kuvaus2TextView.setVisibility(View.GONE);
    	
    	Button startServiceButton = (Button) findViewById(R.id.startService_button);
    	startServiceButton.setVisibility(View.VISIBLE);
    	
        TextView lokinimi2TextView = (TextView) findViewById(R.id.lokinimi2_textView);
        lokinimi2TextView.setVisibility(View.GONE);
        
        TextView lokinimiTextView = (TextView) findViewById(R.id.lokinimi_textView);
        lokinimiTextView.setVisibility(View.GONE);
        
    }
    
    
    //*************************************************
    // Create new log button
    // ************************************************
    public void routeOnMapButtonClick(View view){
    	Intent i = new Intent(getBaseContext(), RouteOnMapActivity.class);
    	if(mBound){
    		i.putExtra("FILENAME", gpsLocationService.getCurRouteFileName());
    	}
    	else{
    		i.putExtra("FILENAME", "");
    	}
        startActivity(i);
    }
    
    
    //*************************************************
    // Open MapActivity to edit geofences
    // (password needed)
    // *************************************************
    public void setOnMapButtonClick(View view){
    	
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);

    	alert.setTitle("Salasana");
    	alert.setMessage("Sy�t� salasana");

    	// Password input
    	final EditText password = new EditText(this);
    	alert.setView(password);

    	// Listeners
    	alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	    	public void onClick(DialogInterface dialog, int whichButton) {
	    		String pass = password.getText().toString();
	    		
	    		if(pass.equals(Parameters.PASSWORD) ){
	    	    	// Start activity
	    	    	Intent i = new Intent(getBaseContext(), MapActivity.class);
	    	        startActivity(i);
	    		}
    	  	}
    	});
    	alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    	    // Do nothing if canceled
    		}
    	});

    	alert.show();
    }
    
    
    //*************************************************
    // Callback for binding/unbinding to the service
    // *************************************************
    private ServiceConnection mConnection = new ServiceConnection() {

    	// When bound to the service
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalBinder binder = (LocalBinder) service;
            gpsLocationService = binder.getService();
            mBound = true;

            // Create new log file
            gpsLocationService.createNewLogFile(logDescription, driver, vehicle, cargoDetails, cargoNumber);
            
            // Show name for a log file on UI
            curLogFileName = gpsLocationService.getCurLogFileName();
            TextView lokinimi2TextView = (TextView) findViewById(R.id.lokinimi2_textView);
            lokinimi2TextView.setText(curLogFileName);
            lokinimi2TextView.setVisibility(View.VISIBLE);
            
            Toast.makeText(getApplicationContext(), "Paikannus k�ynnistetty", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Toast.makeText(getApplicationContext(), "Paikannus pys�ytetty", Toast.LENGTH_SHORT).show();
        }
    };

    
    @Override
    public void onStart() {
        super.onStart();
        
    }

    
    @Override
    protected void onPause() {
        super.onPause();

    }

    
    @Override
    protected void onResume() {
        super.onResume();

    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	// Unbind the service
    	if (mBound) {
	    	unbindService(mConnection);
	        mBound = false;
	        Toast.makeText(getApplicationContext(), "Paikannus pys�ytetty", Toast.LENGTH_SHORT).show();
    	}
    }
   
}
