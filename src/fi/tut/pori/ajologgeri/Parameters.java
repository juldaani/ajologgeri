// Sovelluksen ohjausparametrit
// TTY, Porin laitos / Juho Uusi-Luomalahti			2.7.2014

package fi.tut.pori.ajologgeri;

public class Parameters {
	
	// time [ms] between location updates received from location manager
	protected static final long LOC_UPDATE_INTERVAL = 3000;
	
	// length of filtering window (cannot be greater than PREV_LOCATIONS_LIST_LEN)
	protected static final int FILTER_WINDOW_LEN = 3;
	
	// Length of list of previous locations.
	protected static final int PREV_LOCATIONS_LIST_LEN = 20;
	
	// maximun assumed speed [m/s]
	protected static final int MAX_SPEED = 60;
	
	// minimum acceptable accuracy (radius of 68% confidence) [m]
	//protected static final int MIN_ACCURACY = 50;
			
	// radius for analyzing is mobile device moving (if enough points are inside the radius then device is still) [m]
	protected static final int STOP_RADIUS = 10;
	
	// number of points which should lie inside the STOP_RADIUS we can conclude that device is still
	// (should be less than PREV_LOCATIONS_LIST_LEN)
	protected static final int POINT_QTY_STOPPED = 7;
	
	// Length of the routePointList (GpsLocationService)
	// When size of the routePointList has reached this length, then objects in the list are written to 
	// the route file
	protected static final int ROUTEPOINT_LIST_LEN = 5;

	// Dropbox account keys
	protected static final String APP_KEY = " DB_ACCOUNT_DETAILS_HERE ";
	protected static final String APP_SECRET = " DB_ACCOUNT_DETAILS_HERE ";
	
	// Max cache size in the local file system for cached files (for dropbox syncing) [bytes]
	protected static final int MAX_CACHE = 10000000;
	
	// Password for setting geofences
	protected static final String PASSWORD = "salasana";
}
