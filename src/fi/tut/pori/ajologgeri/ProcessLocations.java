// Luokka sisältää koodin gps-datan prosessoimiseksi
// (suodatus, pysähtymisen analysointi, matkan mittaus... )
// TTY, Porin laitos / Juho Uusi-Luomalahti			2.7.2014


package fi.tut.pori.ajologgeri;

import java.util.LinkedList;

import android.location.Location;


public class ProcessLocations {
	
	private LinkedList<Location> locationList, filteredLocList;		// List of previous locations
	
	// Maximum possible distance travelled between location updates.
	// If the distance is greater than maxDistDelta --> interpreted as an outlier
	private int maxDistDelta;
	
	
	// *****************************
	// Konstruktori
	// ******************************
	public ProcessLocations(){
		locationList = new LinkedList<Location>();
		filteredLocList = new LinkedList<Location>();
		maxDistDelta = (Parameters.MAX_SPEED * (int)Parameters.LOC_UPDATE_INTERVAL)/1000;
	}
	
	
	// *********************************
	// Interface for accessing to the class
	// ********************************
	public AnalyzedData inputLocation(Location location, LinkedList<Geofence> geofenceList){
		
		// If locationList IS NOT full, then add location to the first position
		if( (location != null) && (locationList.size() < Parameters.PREV_LOCATIONS_LIST_LEN) ){
			locationList.addFirst(location);
		}
		// If locationList IS full, then add to first and remove last
		else if( (location != null) ){
			locationList.addFirst(location);
			locationList.removeLast();
		}
		
		// Filter new sample
		Location filteredLoc = locationFilter();		
		
		// If filteredLocList IS NOT full, then add location to the first position
		if( (filteredLoc != null) && (filteredLocList.size() < Parameters.PREV_LOCATIONS_LIST_LEN)){
			filteredLocList.addFirst(filteredLoc);
		}
		// If filteredLocList IS full, then add to first and remove last
		else if( filteredLoc != null ){
			filteredLocList.addFirst(filteredLoc);
			filteredLocList.removeLast();
		}
		
		boolean isMoving = analyzeIsMoving();
		double distBetweenCurAndPrevLoc = calcDistBetweenCurAndPrevLoc() / 1000;	// in kilometers
		AnalyzedData locationData = new AnalyzedData(filteredLocList.getFirst(), isMoving, false, "",
				"", distBetweenCurAndPrevLoc);
		
		// Find out if device is stopped inside some geofence
		// Execute method only if device is still (no waste calculations)
		if(isMoving == false){
			// If device isn't moving, then return zero distance between cur and prev locations
			locationData = new AnalyzedData(filteredLocList.getFirst(), isMoving, false, "", "", 0.0);
			
			// If inside some geofence then put information about the geofence to the locationData
			Geofence curGeof = analyzeIsInsideGeofence(filteredLocList.getFirst(), geofenceList);
			if(curGeof != null){
				locationData = new AnalyzedData(filteredLocList.getFirst(), isMoving, true, curGeof.getPlace(), 
						curGeof.getActivity(), 0.0);
			}
		}

		// Return filtered data
		return locationData;
	}
	
	
	
	//**********************************************************
	// Method returns distance between current and previous locations
	//*************************************************************
	private double calcDistBetweenCurAndPrevLoc(){
		if( filteredLocList.size() > 1){
			Location curLoc = filteredLocList.getFirst();
			Location prevLoc = findNthSample(1, filteredLocList);
			double dist = curLoc.distanceTo(prevLoc);
			return dist;
		}
		
		return 0.0;
	}
	
	
	//*********************************************
	// Method checks if device is inside some geofence
	//**********************************************
	private Geofence analyzeIsInsideGeofence(Location filteredLoc, LinkedList<Geofence> geofenceList){
		for (Geofence geof : geofenceList) {	//iterate through list
			// Create new Location object because distanceTo method requires it as an input
			Location tmpLocation = new Location("tmpLoc");
			tmpLocation.setLatitude(geof.getLatLng().latitude);
			tmpLocation.setLongitude(geof.getLatLng().longitude);
			
			int geofRad = geof.getRadius();
			double dist2GeofCenterpoint = filteredLoc.distanceTo(tmpLocation);
			
			// If the distance between device's current location and geofence's centerpoint  
			// is less than the geofence's radius, then device is inside the geofence
			if( (dist2GeofCenterpoint) < geofRad){
				return geof;
			}
		}
		return null;
	}
	
	
	// ***************************************
	// Find out if device is moving or still 
	// **************************************
	private boolean analyzeIsMoving(){
		int stopCounter = 0;
		
		// Finds out how many points in the filteredLocList lies inside the STOP_RADIUS.
		// STOP_RADIUS is around the second point in the list.
		if( filteredLocList.size() > 1){
			for (Location loc : filteredLocList) {	//iterate through list
				float dist = findNthSample(1, filteredLocList).distanceTo(loc);	//get dist to second point
				if( dist < Parameters.STOP_RADIUS){
					stopCounter++;
				}
			}
		}
		// If enough points in the list lies inside the STOP_RADIUS --> then device is still
		if( stopCounter >= Parameters.POINT_QTY_STOPPED ){
			return false;
		}
		
		return true;
	}
	
	
	// **************************
	// Filter locations, avg filter
	// **************************
	private double latSum, lonSum, avgLat, avgLon;
	
	private Location prevLocation, avgCurLocation;
	private Location locationFilter(){
		
		// If filtering window is not full, then don't filter
		if(locationList.size() < Parameters.FILTER_WINDOW_LEN){
			return locationList.getFirst();
		}
		
		// Avg filtering
		avgCurLocation = new Location("emptyLoc");
		latSum = 0; 
		lonSum = 0;
		int i = 0;
		for (Location loc : locationList) {		//iterate through the filter (previous locations)
			if(i <= (Parameters.FILTER_WINDOW_LEN - 1) ){
				latSum += loc.getLatitude();
				lonSum += loc.getLongitude();
			}
			else{
				break;
			}
			i++;
		}
		avgLat = latSum / Parameters.FILTER_WINDOW_LEN;
		avgLon = lonSum / Parameters.FILTER_WINDOW_LEN;
		avgCurLocation.setLatitude(avgLat);
		avgCurLocation.setLongitude(avgLon);
		
		// If newest location is outlier then return null
		prevLocation = findNthSample(1, locationList);
		if( locationList.getFirst().distanceTo(prevLocation) > maxDistDelta ){

			// If there is too many consecutive outliers between two non-outlier locations, and the vehicle is moving
			// same time, then the distance between non-outlier locations will grow too large --> 
			// non-outlier locations are interpreted as outliers. So, locationList is cleared when outlier is
			// detected.
			locationList.clear();
			return prevLocation;
		}
		
		return avgCurLocation;
	}
	
	
	// *************************************
	// To find nth sample in a list
	// index is 0 the first, 1 second, 2 third....
	// *************************************
	Location nthLocation;
	
	private Location findNthSample(int n, LinkedList<Location> locList){
		nthLocation = null;
		int i = 0;
		for (Location loc : locList) {		//iterate through the locationList
			if(i == n){
				nthLocation = loc;
			}
			i++;
		}
		return nthLocation;
	}
	
	
}
