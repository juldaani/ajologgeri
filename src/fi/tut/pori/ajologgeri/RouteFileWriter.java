// Kirjoittaa kuljetun reitin tiedostoon
// TTY, Porin laitos / Juho Uusi-Luomalahti		15.7.2014

package fi.tut.pori.ajologgeri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.LinkedList;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

public class RouteFileWriter {

	private String curRouteFileName;
	private LinkedList<RoutePoint> routePointList;		// List for route tracking
	
	
	//*************************
	// Constructor
	//************************
	public RouteFileWriter(String curLogFileName){
		curRouteFileName = "Route_" + curLogFileName +".txt";
		createNewRouteFile();
		routePointList = new LinkedList<RoutePoint>();
		
	}
	
	
    //*************************************
    // Get current date
    //**************************************
    private String getDate(){
    	Calendar c = Calendar.getInstance();
    	int day = c.get(Calendar.DAY_OF_MONTH);
    	int month = c.get(Calendar.MONTH);
    	int year = c.get(Calendar.YEAR);
    	return day + "." +month + "." + year;
    }
	
	
    //***************************************
    // Create new route file
    //***************************************
    private void createNewRouteFile(){
    	File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, curRouteFileName);
		
		try{
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			
			myOutWriter.append("******************************************\n");
			myOutWriter.append("AJOLOKI_ROUTE\t\t" + getDate() + "\n");
			myOutWriter.append("******************************************\n");
			
			myOutWriter.close();
			fOut.close();
		}
		catch(Exception e){}
    }
    
    
    //*****************************************
    // Write to the route file
    //********************************************
    public void write2RouteFile(Context context){
    	// Write on the route file
		File externalStorageDir = Environment.getExternalStorageDirectory();
    	File dir = new File (externalStorageDir + "/lokit/");
    	dir.mkdirs();
		File myFile = new File(dir, curRouteFileName);
		try {
			FileOutputStream fOut = new FileOutputStream(myFile, true);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			
			for (RoutePoint routP : routePointList) {	// iterate through list
				double lat = routP.getLoc().getLatitude();
				double lon = routP.getLoc().getLongitude();
				
				// If device has been stopped in that point
				// @ symbol means stop 
				if( routP.isStopPoint() ){
					myOutWriter.append("@" + lat + "#" + lon + "# " + routP.getPlace() + "# " + routP.getActivity() 
							+ "# " + routP.getStopStartTime() + "# " + routP.getStopEndTime() + "#\n");
				}
				
				// If device is moving
				else{
					myOutWriter.append("#" + lat + "#" + lon + "#\n");
				}
			}
			
			myOutWriter.close();
			fOut.close();
	        
		} catch (Exception e) {
			Toast.makeText(context, "Reittitiedostoon kirjoitus epäonnistui", Toast.LENGTH_SHORT).show();
		}
    }
    
    
    //*******************
    // Add new route point to the list
    //******************
    public void addRoutePoint2List(Context context, RoutePoint routePoint){
    	routePointList.add(routePoint);
    	
        // Write route points periodically to the file.
        // Period depends of the ROUTEPOINT_LIST_LEN
        // After writing route points to the route file routePointList is cleared
        if( routePointList.size() > Parameters.ROUTEPOINT_LIST_LEN ){
        	write2RouteFile(context);
        	routePointList.clear();		// Clear list
        }
    }
    
    
    public String getRouteFileName(){
    	return curRouteFileName;
    }
    
}
