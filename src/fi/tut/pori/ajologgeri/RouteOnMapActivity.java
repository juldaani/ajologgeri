// Activity kuljetun reitin piirtämiseksi kartalle.
// TTY, Porin laitos / Juho Uusi-Luomalahti			24.7.2014

package fi.tut.pori.ajologgeri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.StringTokenizer;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

public class RouteOnMapActivity extends Activity implements OnInfoWindowClickListener {
	
    private GoogleMap map;
    
    private String filename;
    
    private LinkedList<RoutePoint> routePointList;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_map);
 
		Intent i = getIntent();
		filename = i.getStringExtra("FILENAME");

		routePointList = new LinkedList<RoutePoint>();
	
		initMap();
		
		parseRouteFile();
		add2Map();
    }
    
    
    //*************************************
    // Add route and stops on the map
    //*************************************
    private void add2Map(){
    	 
    	 PolylineOptions lineOpt = new PolylineOptions()
    	 	.color(Color.BLUE)
			.width(4.0f);
    	 
    	 for (RoutePoint routP : routePointList) {		
    		 LatLng latLng = new LatLng( routP.getLoc().getLatitude(), routP.getLoc().getLongitude() );
    		 
    		 if( routP.isStopPoint() == false ){
	    		 //pointList.addFirst(latLng);
    			 lineOpt.add(latLng);
    		 }
    		 else{
    			 MarkerOptions markerOptions = new MarkerOptions()
			     	.position(latLng)
			     	.title(routP.getPlace())
			     	.snippet(routP.getActivity() +", " + routP.getStopStartTime() + " - " + routP.getStopEndTime());
    			 map.addMarker(markerOptions);
    		 }
    	 }
    	 
    	 Polyline polyline = map.addPolyline(lineOpt);
         
    }
    
    
    //**********************************
    // Parse information from a route file
    //***********************************
    private void parseRouteFile(){
    	
    	try{
    		File externalStorageDir = Environment.getExternalStorageDirectory();
        	File dir = new File (externalStorageDir + "/lokit/");
        	
    		File myFile = new File(dir, filename);
    		FileReader fileReader = new FileReader(myFile);
    		BufferedReader bufferedReader = new BufferedReader(fileReader);
    		
    		String readLine;
		
		while((readLine = bufferedReader.readLine()) != null){
			
			// Read first character
			String firstChar = null;
			if( (readLine != null) && (readLine.length() > 2) ){
				firstChar = readLine.substring(0, 1);
			}
			
			// If the first character is equals to @, then a line contains information about stop
			if( firstChar.equals("@") ){
				// Parse
				StringTokenizer st = new StringTokenizer(readLine, "#", false);
				String tmpLat = st.nextToken();
				tmpLat = tmpLat.substring(1);
				double lat = Double.parseDouble(tmpLat);
				double lon = Double.parseDouble(st.nextToken());
				String place = st.nextToken();
				String activity = st.nextToken();
				String stopStartTime = st.nextToken();
				String stopEndTime = st.nextToken();
				
				// Add point to list
				Location loc = new Location("tmp");
				loc.setLatitude(lat);
				loc.setLongitude(lon);
				RoutePoint routePoint = new RoutePoint(loc, true, place, activity, stopStartTime, stopEndTime);
				routePointList.addFirst(routePoint);
			}
			
			// If the first character is equals to #, then a line contains information about
			// some coordinate point (means that device was currently moving)
			else if( firstChar.equals("#") ){
				// Parse
				StringTokenizer st = new StringTokenizer(readLine, "#", false);
				double lat = Double.parseDouble(st.nextToken());
				double lon = Double.parseDouble(st.nextToken());
				
				// Add point to list
				Location loc = new Location("tmp");
				loc.setLatitude(lat);
				loc.setLongitude(lon);
				RoutePoint routePoint = new RoutePoint(loc, false, "", "", "", "");
				routePointList.addFirst(routePoint);
			}
		}
		
		bufferedReader.close();
    	}
    	catch(Exception e){
    		Toast.makeText(getApplicationContext(),
                    "Tiedoston avaus epäonnistui.", Toast.LENGTH_SHORT).show();
    	}
    }
   
    
 
    //*********************************************
    // function to load a map
    //*********************************************
    private void initMap() {
        if (map == null) {
        	map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map2)).getMap();
        	map.setOnInfoWindowClickListener(this);
        	
            // check if map is created successfully or not
            if (map == null) {
                Toast.makeText(getApplicationContext(),
                        "Kartan luominen epäonnistui", Toast.LENGTH_SHORT).show();
            }
            else{
                // Map settings
                map.setMyLocationEnabled(true); 		//users current location
                map.getUiSettings().setCompassEnabled(true);		//compass
                map.getUiSettings().setMyLocationButtonEnabled(true);	//mylocation button
                
                
            }
        }
    }
    
    /*
    //***************************************
    // Create markers on a map
    //*************************************
    private void createMarkers(){
    	if(geofenceList != null){
    		for (Geofence curGeof : geofenceList) {		//iterate through the locationList
        		MarkerOptions markerOptions = new MarkerOptions()
			     	.position(curGeof.getLatLng())
			     	.title(curGeof.getPlace())
			     	.snippet(curGeof.getActivity() +", "+ curGeof.getRadius() + " m");
        		map.addMarker(markerOptions);
    		}
    	}
    }*/
    
 
    @Override
    protected void onResume() {
        super.onResume();
        initMap();
    }
    
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    }


	@Override
	public void onInfoWindowClick(Marker arg0) {
		// TODO Auto-generated method stub
		
	}
        
}

