// Luokka reittipisteille. 
// TTY, Porin laitos / Juho Uusi-Luomalahti			24.7.2014

package fi.tut.pori.ajologgeri;

import android.location.Location;

public class RoutePoint extends AnalyzedData{
	
	private boolean isStopPoint;
	
	private String stopStartTime, stopEndTime;

	//**********************
	// Constructors
	//************************
	public RoutePoint(Location loc, boolean isStopPoint, String place, String activity, String stopStartTime,
			String stopEndTime) {
		super(loc, place, activity);
		this.isStopPoint = isStopPoint;
		this.stopStartTime = stopStartTime;
		this.stopEndTime = stopEndTime;
	}
	
	public boolean isStopPoint(){
		return isStopPoint;
	}
	
	public String getStopStartTime(){
		return stopStartTime;
	}
	
	public String getStopEndTime(){
		return stopEndTime;
	}
	
}
